import { writeFileSync, readFileSync, existsSync } from "fs";
import axios from "axios";
import jwkToPem from "jwk-to-pem";
import keyCloakBackend from "keycloak-backend";
import jwt from "jsonwebtoken";

const {
	REALM_NAME,
	AUTH_SERVER_URL,
	AUTH_SERVER_PORT,
	AUTH_SERVER_PROTOCOL,
	CLIENT_ID,
	PEM_CERTIFICATE_PATH,
} = process.env;

const realmName = REALM_NAME;
const authServerPort = AUTH_SERVER_PORT;
const authServerProtocol = AUTH_SERVER_PROTOCOL;
const authServerBaseUrl = AUTH_SERVER_URL;
const authServerUrl = `${authServerProtocol}://${authServerBaseUrl}:${authServerPort}`;
const certificateUrl = `${authServerUrl}/auth/realms/`;
const pemCertificatePath = PEM_CERTIFICATE_PATH;
const clientId = CLIENT_ID;

const keyCloak = keyCloakBackend({
	realm: realmName,
	client_id: clientId,
	"auth-server-url": authServerUrl,
});

export default class Auth {
	constructor() {
		this.initializeCertificate();
		this.isCertificateReinitialized = false;
	}

	initializeCertificate = async () => {
		if (!existsSync(pemCertificatePath)) {
			(async () => await this.updateCertificate())();
		}
	};

	user = async (token, method) => {
		try {
			this.isCertificateReinitialized = false;
			const user = await this.verifyToken(token, method);
			if (user) return { user };
			return null;
		} catch (error) {
			throw new Error(`#Auth #user | message: ${error}`);
		}
	};
	getCertificateOfToken = (token) => {
		try {
			if (!existsSync(pemCertificatePath)) {
				(async () => await this.updateCertificate())();
			}
			const pemCertificateStringified = String(readFileSync(pemCertificatePath));
			const pemCertificate = JSON.parse(pemCertificateStringified);
			const decoded = jwt.decode(token, { complete: true });
			const certId = decoded?.header?.kid;
			return pemCertificate[certId];
		} catch (error) {
			use.throwError.catch(`#Auth #getCertificateOfToken | message: ${error}`);
		}
	};
	verifyToken = async (token, method) => {
		try {
			if (!token || !method) {
				throw new Error(`#verifyToken | message: if (!token || !method)`);
			}

			let tokenVerified = false;

			if (method === "GET") {
				tokenVerified = await keyCloak.jwt.verifyOffline(token, this.getCertificateOfToken(token));
			} else {
				tokenVerified = await keyCloak.jwt.verify(token);
			}

			if (
				tokenVerified &&
				tokenVerified.content &&
				tokenVerified.content.exp &&
				this.isTokenNotExpired(tokenVerified.content.exp)
			) {
				this.isCertificateReinitialized = false;
				return {
					id: tokenVerified.content.sub,
					username: tokenVerified.content.preferred_username,
					email: tokenVerified.content.email,
				};
			}

			//throw new Error(`#verifyToken | message: Token Not Verified`);
			return null;
		} catch (error) {
			try {
				console.log("error.messageZXCVBNM", error.message, error);
				if (!this.isCertificateReinitialized) {
					this.isCertificateReinitialized = true;
					await this.updateCertificate();
					return await this.verifyToken(token, method);
				}
			} catch (error) {
				console.log("?????ERRORRR????", error);
				return null;
			}
			return null;
			// throw new Error(`#Auth #verifyToken | message: ${error}`);
		}
	};

	updateCertificate = async () => {
		try {
			const url = `${certificateUrl}${realmName}/protocol/openid-connect/certs`;
			const jsonCertificate = await axios.get(url);
			const pemCertificate = {};
			jsonCertificate.data.keys.forEach((key) => {
				const certKey = key.kid;
				pemCertificate[certKey] = jwkToPem(key);
			});
			writeFileSync(pemCertificatePath, JSON.stringify(pemCertificate));
			return true;
		} catch (error) {
			throw new Error(`#Auth #updateCertificate | message: ${error}`);
		}
	};

	isTokenNotExpired = (expiryDate) => {
		return Date.now() < expiryDate * 1000;
	};
}
