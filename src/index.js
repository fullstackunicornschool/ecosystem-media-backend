import app from "./app.js";

const { PORT_EXPRESS_SERVER } = process.env;

app.listen(PORT_EXPRESS_SERVER, () => {
	console.log(`Express Media Server Listening on port ${PORT_EXPRESS_SERVER}`);
});
