// AUTHENTICATION ENABLED -------------------

import statusMessage from "./tools/statusMessage.js";
import Auth from "./Auth.js";
const auth = new Auth();

const middleware = async (request, response, next) => {
	try {
		if (process.env.TEST_ENVIRONMENT && process.env.TEST_ENVIRONMENT === "Active") {
			request.user = "User-Test-Id";
			return next();
		}
		const HEADER_NAME = "authorization";
		const method = request.method;
		const authToken = request.headers[HEADER_NAME];
		const token =
			typeof authToken === "string" && authToken.startsWith("Bearer ")
				? authToken.replace("Bearer ", "")
				: false;
		if (!token) return response.status(401).send(statusMessage[401]);
		const user = await auth.user(token, method);
		if (!user) return response.status(401).send(statusMessage[401]);
		request.user = user.user.id;
		return next();
	} catch (error) {
		return response.status(400).send(`${statusMessage[400]}, ${error}`);
	}
};
export default middleware;

// // AUTHENTICATION DISABLED ------------------

// import statusMessage from "./tools/statusMessage.js";
        
// const middleware = async (request, response, next) => {
// 	try {
// 		if (process.env.TEST_ENVIRONMENT && process.env.TEST_ENVIRONMENT === "Active") {
// 			request.user = "User-Test-Id";
// 			return next();
// 		}
// 		request.user = "User-Test-Id";
// 		return next();
// 	} catch (error) {
// 		return response.status(400).send(`${statusMessage[400]}, ${error}`);
// 	}
// };
// export default middleware;