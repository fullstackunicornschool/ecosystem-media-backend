import express from "express";
import Picture from "./resolvers/Picture.js";
import middleware from "./middleware.js";

const router = express.Router();
const picture = new Picture();

// fetch pictures
router.get("/", middleware, picture.fetch);

// upload new picture
router.post("/new", middleware, picture.new);

export default router;
